#include <linux/perm.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <asm/current.h>

#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/freezer.h>
#include <linux/vmalloc.h>
#include <linux/msg.h>
#include <linux/linkage.h>
#include <linux/unistd.h>
#include <linux/ctype.h>
#include <asm/current.h>
#include <linux/string.h>
#include <linux/nsproxy.h>

#include <linux/perm.h>

#include <linux/slab.h>
#include <linux/list.h>

static char *mystrdup(const char *s) {
	char *b;
	if (!(b = kmalloc(strlen(s) + 1, GFP_KERNEL)))
		return NULL;
	strcpy(b, s);
	return b;
}

static hash_size def_hashfunc(const char *key) {
	hash_size hash = 0;

	while (*key)
		hash += (unsigned char) *key++;

	return hash;
}

HASHTBL *hashtbl_create(hash_size size, hash_size (*hashfunc)(const char *)) {
	HASHTBL *hashtbl;

	if (!(hashtbl = kmalloc(sizeof(HASHTBL), GFP_KERNEL)))
		return NULL;

	if (!(hashtbl->nodes = kcalloc(size, sizeof(struct hashnode_s*), GFP_KERNEL))) {
		kfree(hashtbl);
		return NULL;
	}

	hashtbl->size = size;

	if (hashfunc)
		hashtbl->hashfunc = hashfunc;
	else
		hashtbl->hashfunc = def_hashfunc;

	return hashtbl;
}

void hashtbl_destroy(HASHTBL *hashtbl) {
	hash_size n;
	struct hashnode_s *node, *oldnode;

	for (n = 0; n < hashtbl->size; ++n) {
		node = hashtbl->nodes[n];
		while (node) {
			kfree(node->key);
			oldnode = node;
			node = node->next;
			kfree(oldnode);
		}
	}
	kfree(hashtbl->nodes);
	kfree(hashtbl);
}

int hashtbl_insert(HASHTBL *hashtbl, const char *key, int data) {
	struct hashnode_s *node;
	struct nodelist *temp = NULL;
//	struct nodelist *temp1 = NULL;

	hash_size hash = hashtbl->hashfunc(key) % hashtbl->size;

	/*	fprintf(stderr, "hashtbl_insert() key=%s, hash=%d, data=%s\n", key, hash, (char*)data);*/

	node = hashtbl->nodes[hash];
	while (node) {
		if (!strcmp(node->key, key)) {
			node->data += data;
			return 0;
		}
		node = node->next;
	}

	if ((!(node = kmalloc(sizeof(struct hashnode_s), GFP_KERNEL))) || (!(temp =
			kmalloc(sizeof(struct nodelist), GFP_KERNEL))))
		return -1;
	if (!(node->key = mystrdup(key))) {
		kfree(node);
		return -1;
	}
	node->data = data;
	node->next = hashtbl->nodes[hash];
	hashtbl->nodes[hash] = node;

	temp->node = node;


	printk("hashtbl insert1\n");
	if (current->packageList == NULL) {
		temp->next = NULL;
	//	printk("hashtbl insert2 current->packageList is NULL %p\n", current->packageList);
	} else {
		current->packageList;
		temp->next = current->packageList;
	//	printk("hashtbl insert2 package !=%s\n", node->key);
	}

	current->packageList = temp;
/*
	if (current->packageList == NULL)
		printk("hashtbl insert3 current->packageList is NULL\n");
	else
		printk("hashtbl insert3 =%s\n", current->packageList->node->key);

	if(current->packageSet == NULL)
		printk("hashtbl insert3 packageSet==NULL is impossible\n");
*/
	return 0;
}

int hashtbl_remove(HASHTBL *hashtbl, const char *key) {
	struct hashnode_s *node, *prevnode = NULL;
	hash_size hash = hashtbl->hashfunc(key) % hashtbl->size;

	node = hashtbl->nodes[hash];
	while (node) {
		if (!strcmp(node->key, key)) {
			if (node->data > 0) {
				node->data--;
			}
			//	kfree(node->key);
			//	if(prevnode) prevnode->next=node->next;
			//	else hashtbl->nodes[hash]=node->next;
			//	kfree(node);
			return 0;
		}
		prevnode = node;
		node = node->next;
	}

	return -1;
}

int hashtbl_get(HASHTBL *hashtbl, const char *key) {
	struct hashnode_s *node;
	hash_size hash = hashtbl->hashfunc(key) % hashtbl->size;

	/*	fprintf(stderr, "hashtbl_get() key=%s, hash=%d\n", key, hash);*/

	node = hashtbl->nodes[hash];
	while (node) {
		if (!strcmp(node->key, key))
			return node->data;
		node = node->next;
	}

	return 0;
}
int hashtbl_resize(HASHTBL *hashtbl, hash_size size) {
	HASHTBL newtbl;
	hash_size n;
	struct hashnode_s *node, *next;

	newtbl.size = size;
	newtbl.hashfunc = hashtbl->hashfunc;

	if (!(newtbl.nodes = kcalloc(size, sizeof(struct hashnode_s*), GFP_KERNEL)))
		return -1;

	for (n = 0; n < hashtbl->size; ++n) {
		for (node = hashtbl->nodes[n]; node; node = next) {
			next = node->next;
			hashtbl_insert(&newtbl, node->key, node->data);
			hashtbl_remove(hashtbl, node->key);

		}
	}

	kfree(hashtbl->nodes);
	hashtbl->size = newtbl.size;
	hashtbl->nodes = newtbl.nodes;

	return 0;
}

/*
 * COMPAC
 *
 */

#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/freezer.h>
#include <linux/vmalloc.h>
#include <linux/msg.h>
#include <linux/linkage.h>
#include <linux/unistd.h>
#include <linux/ctype.h>
#include <asm/current.h>
#include <linux/string.h>
#include <linux/nsproxy.h>

#include <linux/perm.h>

#include <linux/slab.h>
#include <linux/list.h>

extern struct pkg_policy_list pkgpolicy;

extern HASHTBL *hashtbl_create(hash_size size,
		hash_size (*hashfunc)(const char *));
extern void hashtbl_destroy(HASHTBL *hashtbl);
extern int hashtbl_insert(HASHTBL *hashtbl, const char *key, int data);
extern int hashtbl_remove(HASHTBL *hashtbl, const char *key);
extern int hashtbl_get(HASHTBL *hashtbl, const char *key);
extern int hashtbl_resize(HASHTBL *hashtbl, hash_size size);

void addPolicytoList(uid_t uid, const char* package, int len,
		char permissions[STR_X][STR_Y], int size) {
	struct pkg_policy_list *new, *tmp;
	int i = 0;
	list_for_each_entry_reverse(tmp, &pkgpolicy.list, list)
	{
		if(uid == tmp->uid)
		{
			for(i = 0;i<= tmp->pkg_count; i++)
			{
				if(!strcmp(tmp->pkgPermission[i].package_name,package))
				{
					freeList(tmp->pkgPermission[i].permission_set);
					tmp->pkgPermission[i].permission_set=(struct permissionset*)kcalloc(1,sizeof(struct permissionset),GFP_KERNEL);
					tmp->pkgPermission[i].permission_set = addPermtoList(tmp->pkgPermission[i].permission_set,permissions,size);
					return;
				}
			}
			tmp->pkg_count++;
			if(tmp->pkg_count >= PERM_LENGTH)
			{
				tmp->pkg_count = PERM_LENGTH;
				printk("warning : package buffer is full \n");
				break;
			}
			memset(tmp->pkgPermission[tmp->pkg_count].package_name,0,PLENGTH);
			strncpy(tmp->pkgPermission[tmp->pkg_count].package_name,package,len);
			tmp->pkgPermission[tmp->pkg_count].permission_set=(struct permissionset*)kcalloc(1,sizeof(struct permissionset),GFP_KERNEL);
			tmp->pkgPermission[tmp->pkg_count].permission_set = addPermtoList(tmp->pkgPermission[tmp->pkg_count].permission_set,permissions,size);
#ifdef COMPAC_DEBUG
			printk("addPolicytoList horizon %d, uid :%d  ,str: %s\n", tmp->pkg_count,tmp->uid, tmp->pkgPermission[tmp->pkg_count].package_name);
#endif
			return;
		}
	}
	new = (struct pkg_policy_list *) kcalloc(1, sizeof(struct pkg_policy_list),
			GFP_KERNEL);
	new->uid = uid;
	new->pkg_count = 0;
	memset(new->pkgPermission[new->pkg_count].package_name, 0, PLENGTH);
	strncpy(new->pkgPermission[new->pkg_count].package_name, package, len);
	new->pkgPermission[new->pkg_count].permission_set =
			(struct permissionset*) kcalloc(1, sizeof(struct permissionset),
					GFP_KERNEL);
	new->pkgPermission[new->pkg_count].permission_set = addPermtoList(
			new->pkgPermission[new->pkg_count].permission_set, permissions,
			size);
	list_add(&(new->list), &(pkgpolicy.list));

#ifdef COMPAC_DEBUG
	printk("addPolicytoList vertical   %d, uid:%d  ,package: %s, str is %s\n",
			new->pkg_count, new->uid,
			new->pkgPermission[new->pkg_count].package_name,
			new->pkgPermission[new->pkg_count].permission_set->permissions);
#endif
	return;
}

struct permissionset* addPermtoList(struct permissionset* cur,
		char permissions[STR_X][STR_Y], int size) {
	struct permissionset *x_tmp = cur;
	int str_index = 0;
	int flag = -1;

	while (str_index < STR_X) {
		if (str_index == 0)
			flag = 0;
		else
			flag = 1;
		x_tmp = addtoList(x_tmp, permissions[str_index],
				strlen(permissions[str_index]) + 1, flag);
#ifdef COMPAC_LISTDEBUG
		printk("addPermtoList  str_index %d, perm %s\n",str_index,permissions[str_index]);
#endif
		str_index++;
	}
	return x_tmp;
}

struct permissionset* addtoList(struct permissionset *cur, char *permissions,
		int size, int flag) {
	struct permissionset *header, *temp, *s;
	header = cur;
	s = cur;
	if (!flag) {
		temp = (struct permissionset*) kcalloc(1, sizeof(struct permissionset),
				GFP_KERNEL);
		temp->permissions = (char *) kcalloc(size, sizeof(char), GFP_KERNEL);
		memcpy(temp->permissions, permissions, size);
		temp->next = NULL;
		header = temp;
#ifdef COMPAC_LISTDEBUG
		printk("null addtoList size %d, perm %s\n",size,header->permissions);
#endif
	} else {
		while (s->next != NULL)
			s = s->next;
		temp = (struct permissionset*) kcalloc(1, sizeof(struct permissionset),
				GFP_KERNEL);
		temp->permissions = (char *) kcalloc(size, sizeof(char), GFP_KERNEL);
		memcpy(temp->permissions, permissions, size);
		temp->next = NULL;
#ifdef COMPAC_LISTDEBUG
		printk("addtoList size %d, perm %s\n",size,temp->permissions);
#endif
		s->next = temp;
	}
	return header;
}

void deletebyUid(uid_t uid) {
	struct pkg_policy_list *tmp = NULL;
	struct pkg_policy_list *rel = NULL;
	struct list_head *pos, *n;
	struct permissionset *new = NULL;
	int i;
	int control = 0;

	list_for_each_safe(pos, n, &pkgpolicy.list) {
		tmp= list_entry(pos, struct pkg_policy_list, list);
		if (tmp->uid == uid) {
			rel = tmp;
			control = 1;
			break;
		}
	}
	if (control == 1) {
		for (i = 0; i <= tmp->pkg_count; i++) {
			new = tmp->pkgPermission[i].permission_set;
			if (new == NULL)
				break;
			freeList(new);
		}
		list_del(pos);
		kfree(rel);
	}
	return;
}

struct permissionset *freeList(struct permissionset *header) {
	struct permissionset *tmp, *s, *cur;
	s = header;
	cur = header;
	if (cur->next == NULL) {
		kfree(cur);
		return NULL;
	}
	while (cur->next != NULL) {
		tmp = cur->next;
		cur->next = tmp->next;
		kfree(tmp);
	}
	kfree(cur);
	return NULL;
}

#ifdef LOOSEPOLICY
int checkUid(uid_t uid,const char* package, int len, char *permission, int plen)
{
	struct pkg_policy_list *tmp_policy;
	struct permissionset *tmp_permission;
	int rel;
	if(package == NULL || permission == NULL) return 0;
#ifdef COMPAC_CHECKDEBUG
	printk("check uid %d, package %s\n",uid,package);
#endif
	if((tmp_policy= pkgbyUid(uid))==NULL) return 0;
	if((tmp_permission = permbyPkg(tmp_policy,package,len))==NULL) return 0;
	rel = checkPermission(permission,plen,tmp_permission);
	return rel;
}
#else
// If the package call chain has the permission, return 1;
// else, return 0;
int checkUid(uid_t uid, pid_t tid, HASHTBL *packageSet,
		struct nodelist *packageList, const char *permission, int plen) {
	struct pkg_policy_list *tmp_policy;
	struct permissionset *tmp_permission;
	struct nodelist* temp;
	int rel = 1;

	temp = packageList;

	if (uid <= 1000)
		return 1;

	if (packageSet == NULL) {
		current->packageSet = current->parent->packageSet;
		packageSet = current->parent->packageSet;
		current->packageList = current->parent->packageList;
		temp = current->parent->packageList;
		printk(
				"checkUid(Granted) uid=%d tid=%d, real p_tid=%d, packageSet is NULL\n",
				uid, tid, current->real_parent->pid);
		//	return 1; // This is set for none Chameleon app only.
	}
	if (temp == NULL) {
		printk(
				"checkUid(Granted) uid=%d tid=%d, p_tid=%d, packageList is NULL\n",
				uid, tid, current->parent->pid);
		return 1; // This is set for none Chameleon app only.
	}
	if (permission == NULL) {
		printk("checkUid(Granted) uid=%d tid=%d, permission is NULL\n", uid,
				tid);
		return 1; // This is set for none Chameleon app only.
	}
	if ((tmp_policy = pkgbyUid(uid)) == NULL) {
		printk("checkUid(Granted) uid=%d tid=%d, package list is NULL\n", uid,
				tid);
		return 1; // This is set for none Chameleon app only.
	}
	do { // Implement the access control logic here.
		if (temp->node == NULL) {
			printk("checkUid(Granted) uid=%d tid=%d, node is NULL\n", uid, tid);
			return 1;
		}
		if (temp->node->data > 0) {
			if ((tmp_permission = permbyPkg(tmp_policy, temp->node->key,
					strlen(temp->node->key))) != NULL) {

				rel = checkPermission(permission, plen, tmp_permission);

				if (rel == 0) {
					printk(
							"checkUid(Denied) uid=%d tid=%d, current->package=%s permission=%s, real ptid=%d, ptid=%d\n\n",
							uid, tid, temp->node->key, permission,
							current->real_parent->pid, current->parent->pid);
					return rel;
				}
			}
		}
		temp = temp->next;

	} while (temp != NULL);
	printk(
			"checkUid(Granted) uid=%d tid=%d, permission=%s, real p_tid=%d, p_tid=%d\n",
			uid, tid, permission, current->real_parent->pid,
			current->parent->pid);
	return 1;
}
#endif

struct pkg_policy_list *pkgbyUid(uid_t uid) {
	struct pkg_policy_list *tmp;
	list_for_each_entry_reverse(tmp, &pkgpolicy.list, list)
	{
#ifdef COMPAC_CHECKDEBUG
		printk("pkgbyuid tmp->uid %d, uid %d\n",tmp->uid,uid);
#endif
		if(tmp->uid == uid) return tmp;
	}
	return NULL;
}

struct permissionset *permbyPkg(struct pkg_policy_list *header,
		const char* package, int len) {
	int i = 0;

	if (current->pkgcache != NULL) {
		if (!strcmp(current->pkgcache->package_name, package)) {
			return current->pkgcache->permission_set;
		}
	}

	for (i = 0; i <= header->pkg_count; i++) {
#ifdef COMPAC_CHECKDEBUG
		printk("permbypkg pachage %s, package_name %s\n",package,header->pkgPermission[i].package_name);
#endif
		if (!strcmp(header->pkgPermission[i].package_name, package)) {
			current->pkgcache = &header->pkgPermission[i];
			printk("check cache %s\n", current->pkgcache->package_name);
			return header->pkgPermission[i].permission_set;
		}
	}
	return NULL;
}

/*
 * If the permission tag can be found, return 1;
 * otherwise, return 0
 */
int checkPermission(const char* permission, int len,
		const struct permissionset *perm) {
	struct permissionset *tmp;
	tmp = perm;

	if (tmp == NULL) {
#ifdef COMPAC_CHECKDEBUG
		printk("permission list is %s\n", permission);
#endif
		return 0;
	}

	while (1) {
		// If exist return 1

		if (!strcmp(permission, tmp->permissions))
			return 1;

#ifdef COMPAC_CHECKDEBUG
		printk("permission list is %s %s\n", permission, tmp->permissions);
#endif
		tmp = tmp->next;
		if (tmp == NULL)
			break;
	}
	return 0;
}

asmlinkage long sys_policyM(uid_t uid, const char* package, int len,
		void *permissions, int size, int control) {
	struct pkg_policy_list *tmp, *dtmp;
	struct permissionset *new;
	int i = 0;
	int rel = -2;
	char k_strings[STR_X][STR_Y];

	switch (control) {
	case PM_ADD: //ADD
#ifdef DROIDGURAD_STRDEBUG
	int str_len = 0;
	int str_index = 0;
#endif
		memset(k_strings, 0, sizeof(k_strings));
		if (copy_from_user(k_strings, permissions, size) != 0) {
			return -1;
		}
#ifdef DROIDGURAD_STRDEBUG
		printk("size is %d\n",size);
		while(str_index < STR_X )
		{
			str_len += printk("%s",k_strings[str_index]); // question why 2 more?
			printk("str_len is %d\n",str_len);
			str_index++;
		}

#endif
		addPolicytoList(uid, package, len, k_strings, size);
		break;
	case PM_DEL: //DEL
		deletebyUid(uid);
		printk("freeing item uid= %d\n", uid);
		break;
	case PM_TRA: //TRAVERSE
		list_for_each_entry_reverse(tmp, &pkgpolicy.list, list)
		{
			for(i = 0; i<= tmp->pkg_count; i++)
			{
				printk("uid: %d, name: %s, pkg_index is %d, pkg_count %d\n",tmp->uid,tmp->pkgPermission[i].package_name,i,tmp->pkg_count + 1);
				new = tmp->pkgPermission[i].permission_set;
				if(new == NULL) break;
				while(1)
				{
					printk("permission list is %s\n",new->permissions);
					new = new->next;
					if(new == NULL) break;
				}

			}
		}
		break;
		/*
		 case PM_CHK: //check
		 rel = checkUid(uid,package,strlen(package),"hello",strlen("hello"));
		 printk("rel %d\n",rel);
		 break;
		 */
	case PM_DVM: //DVM
		list_for_each_entry_reverse(tmp, &pkgpolicy.list, list)
		{
			if(tmp->uid == uid)
			{
				dtmp = tmp;
				if(copy_to_user(permissions,dtmp->pkgPermission,(dtmp->pkg_count + 1)*sizeof(struct packagePermission))!=0)
				{
					return -1;
				}
				return dtmp->pkg_count;
			}
		}
		return -1;
		break;
	default:
		break;
	}
	return 1;
}

asmlinkage long sys_setperm(char* packageName, int control) {
	if (current->cred->uid == 0)
		return 188;

	if (control == ADDPACKAGE) {
		/* Put permission information into the current(target) thread */
#ifdef LOOSEPOLICY
		memset(current->packageName,'\0',100);
		strcpy(current->packageName,packageName);
#ifdef COMPAC_DEBUG
		printk("setperm(ADD) pid=%d, tid=%d packageName=%s\n", current->tgid, current->pid, current->packageName);
#endif

#else
		if (current->packageSet == NULL) {
			if (!(current->packageSet = hashtbl_create(PERM_LENGTH, NULL))) {
				//fprintf(stderr, "ERROR: hashtbl_create() failed\n");
				printk("buhuiba\n");
				// exit(EXIT_FAILURE);
			}
		}
		hashtbl_insert(current->packageSet, packageName, 1);
//		if (current->pid > 300)
//			printk("pid=%d, parent pid=%d group pid=%d\n", current->pid,
//					current->real_parent->pid, current->group_leader->pid);
#ifdef COMPAC_DEBUG
		printk("setperm(ADD) uid=%d pid=%d, tid=%d packageName=%s data=%d\n",
				current->cred->uid, current->tgid, current->pid, packageName,
				hashtbl_get(current->packageSet, packageName));
#endif

#endif
	} else {
#ifdef LOOSEPOLICY
		memset(current->packageName, '\0', 100);
#ifdef COMPAC_DEBUG
		printk("setperm(DEL) pid=%d, tid=%d packageName=%s\n", current->tgid, current->pid, current->packageName);
#endif

#else
		hashtbl_remove(current->packageSet, packageName);
#ifdef COMPAC_DEBUG
		printk("setperm(DEL) uid=%d pid=%d, tid=%d packageName=%s data=%d\n",
				current->cred->uid, current->tgid, current->pid, packageName,
				hashtbl_get(current->packageSet, packageName));
#endif

#endif
	}

	return 188;
}

asmlinkage long sys_getperm(pid_t tid, char* permission, int control) {

	if (permission == NULL) {
		printk("getperm tid=%d permission=NULL flag=1\n", tid);
		return 1;
	}
	struct task_struct* permTask = find_task_by_pid_ns(tid,
			current->nsproxy->pid_ns);
	if (permTask != NULL) {

#ifdef LOOSEPOLICY
		printk("getperm tid=%d, packageName=%s, permission=%s\n",permTask->pid, permTask->packageName, permission);
		return !checkUid(permTask->cred->uid, permTask->packageName, strlen(permTask->packageName), permission, strlen(permission));
#else

		int flag = checkUid(permTask->cred->uid, tid, permTask->packageSet,
				permTask->packageList, permission, strlen(permission));
		printk("getperm tid=%d, permission=%s flag=%d\n", permTask->pid,
				permission, flag);
		return flag;
#endif
	}
	printk("getperm tid=%d permission=%s flag=0\n", tid, permission);
	return 0;
}


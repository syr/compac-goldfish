#ifndef _LINUX_PERM_H
#define _LINUX_PERM_H

#include <linux/types.h>
#include <linux/list.h>

/*------------------------------------------------------*/
/* If you want to use loosepolicy, uncomment the following line */
//#define LOOSEPOLICY

/*----------------defined for syscall ------------------*/
#define SETPERM					188
#define GETPERM					189
#define CONTROLPERM				223

#define ADDPACKAGE				0
#define DELPACKAGE				1

/*------------------------------------------------------*/

#define DROIDGUARD
#define DROIDGUARD_DEBUG

#define COMPAC
#define COMPAC_DEBUG
//#define DROIDGUARD_LISTDEBUG
//#define DROIDGUARD_CHECK_DEBUG
//#define DROIDGURAD_STRDEBUG

//#define COMPAC_LISTDEBUG
//#define COMPAC_CHECK_DEBUG
//#define COMPAC_STRDEBUG

#define PLENGTH 100
#define PERM_LENGTH 30
#define PM_ADD 1
#define PM_DEL 2
#define PM_TRA 3
#define PM_CHK 4
#define PM_DVM 5

#define STR_X 4
#define STR_Y 100

typedef size_t hash_size;

struct hashnode_s {
        char *key;
        int data;
        struct hashnode_s *next;
};

typedef struct hashtbl {
        hash_size size;
        struct hashnode_s **nodes;
        hash_size (*hashfunc)(const char *);
} HASHTBL;

struct nodelist {
	struct hashnode_s *node;
	struct nodelist *next;
};

struct permissionset
{
        struct permissionset *next;
        char *permissions;
};


struct packagePermission
{
        char package_name[PLENGTH];
        long hash;
        struct permissionset *permission_set;
};

struct pkg_policy_list
{
	struct list_head list;
	int uid;
	int pkg_count;
	struct packagePermission pkgPermission[PERM_LENGTH];
};
#ifdef LOOSEPOLICY
int checkUid(uid_t uid, const char *package, int len,char* permission, int plen);
#else
int checkUid(uid_t uid, pid_t tid, HASHTBL *packageSet, struct nodelist *packageList, const char *permission, int plen);
#endif
struct pkg_policy_list *pkgbyUid(uid_t uid);
struct permissionset *permbyPkg(struct pkg_policy_list *header, const char* package, int len);
int checkPermission(const char* permission, int len, const struct permissionset *perm);
void addPolicytoList(uid_t uid, const char *package, int len, char permissions[STR_X][STR_Y], int size);
struct permissionset* addPermtoList(struct permissionset* cur, char permissions[STR_X][STR_Y],int size);
struct permissionset* addtoList(struct permissionset *cur,char *permissions,int size, int flag);
void deletebyUid(uid_t uid);
struct permissionset* freeList(struct permissionset* header);


#endif

